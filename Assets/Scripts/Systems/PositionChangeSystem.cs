﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class PositionChangeSystem : ReactiveSystem<GameEntity> 
{
	private readonly GameContext _context;

	public PositionChangeSystem(IContext<GameEntity> context) : base(context)
	{
		_context = (GameContext)context;
	}

	public PositionChangeSystem(ICollector<GameEntity> collector) : base(collector)
	{
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		return context.CreateCollector(GameMatcher.SuggestedPosition);
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.hasSuggestedPosition;
	}

	protected override void Execute(List<GameEntity> entities)
	{
		foreach (var entity in entities)
		{
			var sugPos = entity.suggestedPosition.position;
			var pos = entity.position.position;
			var cells = _context.GetEntitiesWithPosition(sugPos);
			var newCell = cells.FirstOrDefault(e => e.hasPlaced);
			cells = _context.GetEntitiesWithPosition(pos);
			var curCell = cells.FirstOrDefault(e => e.hasPlaced);
			if (newCell != null)
			{
				if (!newCell.placed.IsBusy)
				{
					curCell.ReplacePlaced(-1);
					entity.ReplacePosition(newCell.position.position);
					newCell.ReplacePlaced(entity.creationIndex);
				}
			}
		}
	}
}
