﻿using System.Collections;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class StartPositioningSystem : ReactiveSystem<GameEntity> 
{
    public StartPositioningSystem(IContext<GameEntity> context) : base(context)
    {
    }

    public StartPositioningSystem(ICollector<GameEntity> collector) : base(collector)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Selected);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (entity.isSelected)
            {
                var curPos = entity.position.position;
                entity.AddSuggestedPosition(curPos);
            }
            else
            {
                entity.isSelected = false;
            }
        }
    }
}
