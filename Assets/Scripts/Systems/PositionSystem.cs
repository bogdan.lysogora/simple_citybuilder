﻿using System.Collections;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class PositionNotifyingSystem : ReactiveSystem<GameEntity>
{
    public PositionNotifyingSystem(IContext<GameEntity> context) : base(context)
    {
    }

    public PositionNotifyingSystem(ICollector<GameEntity> collector) : base(collector)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return null;//context.CreateCollector(GameMatcher.Position);
    }

    protected override bool Filter(GameEntity entity)
    {
        return false;//entity.hasPosition && entity.hasPositionListener;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var listeners = entity.positionListener.value;

            foreach (var listener in listeners)
            {
                listener.OnPosition(entity, entity.position.position);
            }
        }
    }
}
