﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class InputMediatorSystem 
{
	private readonly IInputService _inputService;
	private GameContext _gameContext;

	public InputMediatorSystem(IInputService inputService, IContext<GameEntity> context)
	{
		_inputService = inputService;
		_gameContext = (GameContext) context;
		
		_inputService.OnEntityHoverStarted += InputServiceOnEntityHoverStarted;
	}

	private void InputServiceOnEntityHoverStarted(int i)
	{
		var entity = _gameContext.GetEntitiesWithId(i).ElementAt(0);
		if (entity != null && !entity.isHover)
		{
			entity.isHover = true;
		}
	}
}
