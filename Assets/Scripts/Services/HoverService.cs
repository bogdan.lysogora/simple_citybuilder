﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputService
{
	event Action<int> OnEntityHoverStarted;
	event Action<int> OnEntityHoverCompleted;
	
	event Action<int> OnConfirmButtonClicked;
}

public class HoverService : MonoBehaviour, IInputService
{
	public event Action<int> OnEntityHoverStarted;
	public event Action<int> OnEntityHoverCompleted;
	public event Action<int> OnConfirmButtonClicked;
	
	public GameObject CurHoverObject;
	// Use this for initialization
	void Start () {
		
	}

	private void CheckRay()
	{
		RaycastHit hit;

		if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f))
		{
			var go = hit.collider.gameObject;
			if (go != CurHoverObject)
			{
				CurHoverObject = go;

				var view = go.GetComponent<EntityView>();
				if (view != null)
				{
					if (OnEntityHoverStarted != null)
					{
						OnEntityHoverStarted(view.EntityId);
					}
				}
			}
		}
		else
		{
			CurHoverObject = null;
		}
	}
	
	// Update is called once per frame
	void Update () {
		CheckRay();
	}

	
}
