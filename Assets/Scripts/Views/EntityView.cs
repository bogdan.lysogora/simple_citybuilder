﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityView : MonoBehaviour, IPositionListener
{
	public int EntityId;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPosition(GameEntity entity, Vector2Int position)
	{
		EntityId = entity.creationIndex;
		transform.position =  new Vector3(position.x, 0, position.y);
	}
}
