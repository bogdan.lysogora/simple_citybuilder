﻿using Entitas;
using UnityEngine;
using Entitas.CodeGeneration.Attributes;

[Game, Event(true)]
public class PositionComponent : IComponent
{
    [EntityIndex]
    public Vector2Int position;
}
