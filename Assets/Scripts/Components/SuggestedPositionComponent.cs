﻿using Entitas;
using UnityEngine;
using Entitas.CodeGeneration.Attributes;

[Game, Event(true)]
public class SuggestedPositionComponent : IComponent
{
    public Vector2Int position;
}
