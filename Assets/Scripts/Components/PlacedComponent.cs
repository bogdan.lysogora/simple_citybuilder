﻿using Entitas;
using UnityEngine;
using Entitas.CodeGeneration.Attributes;

[Game]
public class PlacedComponent : IComponent
{
    public bool IsBusy{get { return placedEntityId >= 0; }}
    public int placedEntityId;
}
