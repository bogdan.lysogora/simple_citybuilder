﻿using Entitas;
using UnityEngine;
using Entitas.CodeGeneration.Attributes;

[Game]
public class IdComponent : IComponent
{
    [EntityIndex]
    public int id;
}
