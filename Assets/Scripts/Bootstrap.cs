﻿using System.Collections;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
	private Systems _systems;

	public EntityView BuildingAView;
	
	public EntityView BuildingBView;

	private InputMediatorSystem _inputMediator;

	public HoverService _inputService;

	void Start()
	{
		// get a reference to the contexts
		var contexts = Contexts.sharedInstance;
        
		// create the systems by creating individual features
		_systems = new Feature("Systems")
			.Add(new TutorialSystems(contexts));

		// call Initialize() on all of the IInitializeSystems
		_systems.Initialize();
		
		_inputMediator = new InputMediatorSystem(_inputService, contexts.game);
		
		CreateWorld(contexts);
	}

	void CreateWorld(Contexts contexts)
	{
		var building = contexts.game.CreateEntity();
		building.AddId(building.creationIndex);
		building.AddPosition(Vector2Int.zero);
		building.AddSuggestedPosition(Vector2Int.zero);
		building.AddPositionListener(BuildingAView);
		
		building = contexts.game.CreateEntity();
		building.AddId(building.creationIndex);
		building.AddPosition(Vector2Int.right * 2);
		building.AddSuggestedPosition(Vector2Int.right * 2);
		building.AddPositionListener(BuildingBView);
		
		
		// create field
		for (int x = 0; x < 10; x++)
		{
			for (int y = 0; y < 10; y++)
			{
				var cell = contexts.game.CreateEntity();
				cell.AddPosition(new Vector2Int(x - 5, y - 5));
				cell.AddPlaced(-1);
			}
		}
		
		
		
		
		
	}

	void Update()
	{
		// call Execute() on all the IExecuteSystems and 
		// ReactiveSystems that were triggered last frame
		_systems.Execute();
		// call cleanup() on all the ICleanupSystems
		_systems.Cleanup();
	}
}

public class TutorialSystems : Feature
{
	public TutorialSystems(Contexts contexts) : base ("Tutorial Systems")
	{
		Add(new PositionChangeSystem(contexts.game));
		Add(new PositionNotifyingSystem(contexts.game));
	}
}
